FROM bluesquall/android-sdk:sdkman
MAINTAINER https://bitbucket.org/bluesquall/docker-flutter/issues

USER root
RUN apt-get update \
 && apt-get install -y --no-install-recommends \
      git \
      libglu1-mesa \
      xz-utils \
 && apt-get autoremove -y && apt-get clean

USER artoo
WORKDIR ${HOME}

RUN sdkmanager --install \
      'build-tools;30.0.3' \
      'patcher;v4' \
      'platform-tools' \
      'platforms;android-30' \
      'sources;android-30' \
    --sdk_root=${ANDROID_HOME}

ENV FLUTTER_HOME ${HOME}/flutter
ENV PATH "$PATH:${FLUTTER_HOME}/bin"

RUN git clone --depth 1 --branch stable \
    https://github.com/flutter/flutter.git ${FLUTTER_HOME}

RUN flutter doctor
